const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const Product = require('../models/Product');
const auth = require('../middleware/auth');

const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

const createRouter = () => {
  router.get('/', (req, res) => {
    if (req.query.category) {
      Product.find({category: req.query.category})
        .then(products => res.send(products))
        .catch(() => res.sendStatus(500));
    } else {
      Product.find()
        .then(products => res.send(products))
        .catch(() => res.sendStatus(500));
    }
  });

  router.get('/:id', (req, res) => {
    const productId = req.params.id;

    Product.findOne({_id: productId})
      .populate({path:'seller', select: 'displayName phoneNumber username'})
      .then(product => res.send(product));
  });

  router.post('/', [auth, upload.single('image')], (req, res) => {
    if (!req.file) {
      res.send({message: 'Fill all fields'});
    }

    const productData = req.body;

    productData.image = req.file.filename;

    const product = new Product(productData);

    product.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  router.delete('/:id', auth, async (req, res) => {
    const userId = req.user._id;
    const productId = req.params.id;

    const product = await Product.findOne({_id: productId}).populate('seller');

    if (!product) {
      res.send({error: 'You try delete not exist product'});
    }

    if (product.seller._id.equals(userId)) {
      await product.remove();
      res.send({message: 'Product has deleted'});
    } else {
      res.status(403).send({error: 'You can not delete this product'});
    }
  });

  return router;
};

module.exports = createRouter;
const mongoose = require('mongoose');
const config = require('./config');

const User = require('./models/User');
const Product = require('./models/Product');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
  try {
    await db.dropCollection('users');
    await db.dropCollection('products');
  } catch (e) {
    console.log('Collections were not present, skipping drop...');
  }

  const [User1, User2] = await User.create({
    username: 'lolo',
    password: '123',
    displayName: 'Vasya',
    phoneNumber: '996 555 856 855'
  }, {
    username: 'abc',
    password: '123',
    displayName: 'Petya',
    phoneNumber: '996 558 552 201'
  });

  await Product.create({
    title: 'comp1',
    price: 300000,
    description: 'Very cool comp',
    category: 'computers',
    image: 'comp3.png',
    seller: User1._id
  }, {
    title: 'mobile1',
    price: 15000,
    description: 'Some kinda description',
    category: 'mobiles',
    image: 'mobile2.jpg',
    seller: User1._id
  }, {
      title: 'car1',
      price: 190000,
      description: 'Some kinda description',
      category: 'cars',
      image: '3dgFbNFC6E4emNjeYi2SB.jpg',
      seller: User1._id
    }, {
      title: 'comp2',
      price: 10000,
      description: 'Some kinda description',
      category: 'computers',
      image: 'h6B7bxjG_3vAM5FrnLYsT.jpeg',
      seller: User2._id
    }, {
      title: 'mobile2',
      price: 110000,
      description: 'Some kinda description',
      category: 'mobiles',
      image: 'XJEMLOpil2u65udyLJVlV.jpg',
      seller: User2._id
    }, {
      title: 'car2',
      price: 1109000,
      description: 'Some kinda description',
      category: 'cars',
      image: '2sL~XBYvtm1AVInAAkX19.jpeg',
      seller: User2._id
    });

  db.close();
});
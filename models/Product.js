const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProductSchema = new Schema({
  title: {
    type: String, required: true
  },
  price: {
    type: Number, required: true, min: 0
  },
  image: {
    type: String, required: true
  },
  description: {
    type: String, required: true
  },
  category: { // поиск по категории производится
    type: String, required: true // валидировать
  },
  seller: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  }
});

const Product = mongoose.model('Product', ProductSchema);

module.exports = Product;